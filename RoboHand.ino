#include <Servo.h> 

/// Servo pins
/// Middle: 11
///   Left: 10
///  Right: 9
///   Claw: 6

/// Joystick pins
/// X1, Y1: A0, A1
/// X2, Y2: A2, A3

const int SERVOS = 4;

int
  PIN[SERVOS],
  value[SERVOS],
  idle[SERVOS],
  currentAngle[SERVOS],
  MIN[SERVOS], MAX[SERVOS],
  INITANGLE[SERVOS],
  previousAngle[SERVOS],
  ANA[SERVOS];

Servo servos[SERVOS];

// If you want joystick buttons to control the claw uncomment this
//#define SWITCHES

#ifdef SWITCHES
// Joystick buttons
const int SW1Pin = 2, SW2Pin = 3;
boolean SWDown = false;

boolean clawClosed = true;
#endif

void setup() {
  Serial.begin(115200);

  // Init joystick buttons
  #ifdef SWITCHES
  pinMode(SW1Pin, INPUT_PULLUP);
  pinMode(SW2Pin, INPUT_PULLUP);
  #endif

  // Init meArm servos state
  //Middle Servo
  PIN[0] = 11;
  MIN[0] = 0;
  MAX[0] = 180;
  INITANGLE[0] = 90;
  //Left Side
  PIN[1] = 10;
  MIN[1] = 80; // This should bring the lever to just below 90deg to ground
  MAX[1] = 180;
  INITANGLE[1] = 152; // This should bring the lever parallel with the ground
  //Right Side
  PIN[2] = 9;
  MIN[2] = 25;
  MAX[2] = 180;
  INITANGLE[2] = 90;
  //Claw Servo
  PIN[3] = 6;
  MIN[3] = 0;
  MAX[3] = 179;
  INITANGLE[3] = 0;
  
  for (int i = 0; i < SERVOS; i++) {
    servos[i].attach(PIN[i]);
    servos[i].write(INITANGLE[i]);
    value[i] = 0;
    idle[i] = 0;
    previousAngle[i] = INITANGLE[i];
  }
}

void loop() {
  delay(15);

  // Handle joystick buttons (note that INPUT_PULLUP flips LOW and HIGH)
  #ifdef SWITCHES
  if (digitalRead(SW1Pin) == LOW || digitalRead(SW2Pin) == LOW) {
    if (SWDown == false) {
      // Toggle claw
      clawClosed = !clawClosed;
    }
    SWDown = true;
  } else {
    SWDown = false;
  }
  #endif
  
  for (int i = 0; i < SERVOS; i++) {
    value[i] = analogRead(i);
    // If joystick for claw is in center, determine value based on buttons
    #ifdef SWITCHES
    if (i == 3 && value[i] > 512-100 && value[i] < 512+100) value[i] = (clawClosed) ? 0 : 1024;
    #endif
    currentAngle[i] = servos[i].read();
  
    if (value[i] > 512+100) {
      idle[i] = 0;
      if (currentAngle[i] < MAX[i]) ++currentAngle[i];
      if (!servos[i].attached()) servos[i].attach(PIN[i]);
      servos[i].write(currentAngle[i]);     
    } else if (value[i] < 512-100) {
      idle[i] = 0;
      if (currentAngle[i] > MIN[i]) --currentAngle[i];
      if (!servos[i].attached()) servos[i].attach(PIN[i]);
      servos[i].write(currentAngle[i]);    
    } else {
      ++idle[i];
    }

    if (idle[i] > 100) {
      servos[i].detach();
      idle[i] = 0;
    }
  }
}
